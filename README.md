Overview
In this assignment you will extend the capabilities of your current webserver by laying the groundwork for more extensive client/server interactions by implementing POST and CGI.

Specifics of Operation
Running your compiled program will be identical to the output below:
  java -cp . HTTP1Server 3456

Note: Server still needs to support all the features in Project 1: In this assignment you will design a web server in Java that supports a subset of the HTTP 1.0 protocol, the initial full release of the HyperText Transfer Protocol.
Your web server will have to accept incoming socket connections, read a request from the client, and answer with the appropriate HTTP status code.
This assignment is inteded as an extension from your initial 'toy' server to an application closer a deployable service.